package Chess;


public class Tile {

    private ChessPiece piece;
    public enum TileColor{
        Blanc, Noir
    }

    public Tile(TileColor color){
    }

    public Tile(TileColor color, ChessPiece piece){
        this.piece = piece;
    }

    public void setPiece(ChessPiece piece){
        this.piece = piece;
    }

    public ChessPiece getPiece(){
        return this.piece;
    }

    public String getValue(){
        if(piece != null){
            return "[" + piece.getCharValue() + "]";
        } else {
            return "[ ]";
        }
    }

    public boolean isEmpty(){
        return piece == null;
    }

    public void empty(){
        piece = null;
    }
}
